<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;
class SongsController extends Controller
{
    public function displaySongs(){
        return Songs::orderBy('created_at', 'DESC')->get();
    }

    public function store(Request $request){

        $newSongs = new Songs();
        $newSongs->title = $request->title;
        $newSongs->length = $request->length;
        $newSongs->artist = $request->artist;
        $newSongs->save();
        return $newSongs;
    }
}
