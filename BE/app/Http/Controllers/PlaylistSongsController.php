<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Playlist_Song;
class PlaylistSongsController extends Controller
{
    public function displayPlaylistSongs(){
        return Songs::orderBy('created_at', 'DESC')->get();
    }

    public function store(Request $request){

        $newPlaylistSongs = new PlaylistSongs();
        $newPlaylistSongs->title = $request->title;
        $newPlaylistSongs->save();
        return $newPlaylistSongs;
    }
}
