<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongsController;
use App\Http\Controllers\PlaylistsController;
use App\Http\Controllers\PlaylistSongsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');
});//Songs route
Route::get('/songs', [SongsController::class, 'displaySongs']);
Route::post('/upload', [SongsController::class, 'store']);
//Playlist route
Route::get('/playlist', [PlaylistsController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistsController::class, 'store']);
//PlaylistSong route
Route::get('/playlistsongs', [PlaylistSongsController::class, 'displayPlaylistSongs']);
Route::post('/create', [PlaylistSongsController::class, 'store']);

